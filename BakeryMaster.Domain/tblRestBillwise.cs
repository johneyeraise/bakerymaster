﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblRestBillwise
    {
        public int restBillWiseId { get; set; }
        [key]
        public int TblRestBillwise { get; set; }
        public DateTime BillDate { get; set; }
        public int NetAmt { get; set; }
        public string Type { get; set; }
        public int Disp { get; set; }
        public int DisAmt { get; set; }
        public int TotAmt { get; set; }
        public string Month { get; set; }
    }
}
