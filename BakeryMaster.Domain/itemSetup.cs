﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class itemSetup
    {
        public int itemSetupID { get; set; }
        [Key]

        public string ItemName { get; set; }
        public int Rate { get; set; }
        public string ItemNo { get; set; }
        public int Tax { get; set; }
        public string UnitE { get; set; }
        public int PurRate { get; set; }
        public string Kitchen { get; set; }
        public string Unitt { get; set; }
        public string Type { get; set; }
        public string ItemNameT { get; set; }
        public int ItemCode { get; set; }

    }
}
