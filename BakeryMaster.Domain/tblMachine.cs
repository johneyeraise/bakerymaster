﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
  public class tblMachine
    {
        public int ID { get; set; }
        [key]
        public string IPAddress { get; set; }
        public string CName { get; set; }
    }
}
