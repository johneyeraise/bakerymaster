﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public partial class JBNContext : DbContext
    {
        public JBNContext() : base("JBNContext")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.users> Users { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.itemSetup> ItemSetups { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.ledgerTable> LedgerTables { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.resetBill> ResetBills { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.stcokTable> StcokTables { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.supLedger> SupLedgers { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblFix> TblFixes { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblIndent> TblIndents { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblIssue> TblIssues { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblKitchen> TblKitchens { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblOrder>  TblOrders { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblLedger>  TblLedgers { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblMachine>  TblMachines { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblRestBillwise>  TblRestBillwises { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.TblRestItem>  TblRestItems { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblRestPurchase>  TblRestPurchases { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblRestSup>  TblRestSups { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblStock>  TblStocks { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblStockInward>  TblStockInwards { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblUser>  TblUsers { get; set; }
        public System.Data.Entity.DbSet<BakeryMaster.Domain.tblWastage1>  tblWastage1s { get; set; }
    }
}
