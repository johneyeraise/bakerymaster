﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblOrder
    {
        public int OdNo { get; set; }
        [key]
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Prod { get; set; }
        public int Amount { get; set; }
        public int Advance { get; set; }
        public int Bal { get; set; }
        public string Status { get; set; }
        public DateTime DelDate { get; set; }
    }
}
