﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblReceipt
    {
        public int receiptId { get; set; }
        [key]
        public DateTime Date { get; set; }
        public string ExpName { get; set; }
        public string Desc { get; set; }
        public string Payment { get; set; }
        public int Eno { get; set; }
        public int Amount { get; set; }
        public string Under { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public string CompanyName { get; set; }
    }
}
