﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public class TblRestItem
    {
        public int TblRestItemID { get; set; }
        [key]
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public int Itemcode { get; set; }

    }
}
