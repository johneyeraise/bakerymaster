﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblUser
    {
        public int TblUserID { get; set; }
        [key]
        public string UserType { get; set; }
        public string UsrName { get; set; }
        public string Pwd { get; set; }
        public int Admin { get; set; }
        public int Purchase { get; set; }
        public int Issue { get; set; }
        public int Sales { get; set; }
        public int Production { get; set; }
        public int Receipt { get; set; }
        public int Expenses { get; set; }
        public int RecPay { get; set; }
        public int Order { get; set; }
        public int Delivery { get; set; }
        public int Report { get; set; }
        public int BReport { get; set; }
        public int Ret { get; set; }
        public int Payroll { get; set; }
 
    }
}
