﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public class tblIndent
    {
        public int TblIndentID { get; set; }
        [key]
        public int BookNo { get; set; }
        public DateTime Date { get; set; }
        public string ProdName { get; set; }
        public int Qty { get; set; }
        public string Unit { get; set; }
        public string Name { get; set; }
        public string Add { get; set; }
        public string City { get; set; }
    }
}
