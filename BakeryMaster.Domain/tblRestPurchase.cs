﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblRestPurchase
    {
        public int TblRestPurchaseID { get; set; }
        [key]
        public int SCode { get; set; }
        public string SName { get; set; }
        public DateTime Date { get; set; }
        public string Itemname { get; set; }
        public int Rate { get; set; }
        public int Amount { get; set; }
        public int TotAmt { get; set; }
        public int Tax { get; set; }
        public int Discount { get; set; }
        public int NetAmt { get; set; }
        public string Unit { get; set; }
        public int Qty { get; set; }
        public int Itemcode { get; set; }
        public int InvNo { get; set; }
        public string Inv { get; set; }
        public int T { get; set; }


    }
}
