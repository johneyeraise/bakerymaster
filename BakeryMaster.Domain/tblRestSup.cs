﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblRestSup
    {
        public int TblRestSupID { get; set; }
        [key]
        public int supcode { get; set; }
        public string supname { get; set; }
        public string paddr { get; set; }
        public string caddr { get; set; }
        public string phone { get; set; }
        public string area { get; set; }
        public string tngst { get; set; }
        public string cst { get; set; }
      

    }
}
