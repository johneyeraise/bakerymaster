﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblStockInward
    {
        public int TblStockInwardID { get; set; }
        [key]
        public int BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int Qty { get; set; }
        public int Rate { get; set; }
        public int Amount { get; set; }
        public string Type { get; set; }
    }
}
