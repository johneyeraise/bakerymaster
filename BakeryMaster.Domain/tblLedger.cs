﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblLedger
    {
        public int tblLedgerId { get; set; }
        [key]
        public string Cash { get; set; }
        public string Under { get; set; }
    }
}
