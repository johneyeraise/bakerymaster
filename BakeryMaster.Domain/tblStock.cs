﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public class tblStock
    {
        public int TblStockID { get; set; }
        [key]
        public string ItemNo { get; set; }
        public string ItemName { get; set; }
        public string Tamil { get; set; }
        public int Qty { get; set; }
        public int Price { get; set; }
        public string UnitT { get; set; }
        public string UnitE { get; set; }
        public string Type { get; set; }
    }
}
