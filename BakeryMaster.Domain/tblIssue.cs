﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public class tblIssue
    {
        public int TblIssueID { get; set; }
        [key]
        public int Eno { get; set; }
        public DateTime Date { get; set; }
        public string Itemname { get; set; }
        public string Unit { get; set; }
        public int Qty { get; set; }
        public int Itemcode { get; set; }
        public DateTime ItemDate { get; set; }
    }
}
