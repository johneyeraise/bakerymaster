﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
  public  class supLedger
    {
        public int SupLedgerID { get; set; }
        [key]
        public DateTime Date { get; set; }
        public string InvNo { get; set; }
        public int Debit { get; set; }
        public int Credit { get; set; }
        public string SupName { get; set; }
        public int Eno { get; set; }
        public string Type { get; set; }

    }
}
