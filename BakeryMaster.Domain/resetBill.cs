﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class resetBill
    {
        public int reserBillId { get; set; }
        [Key]
        public int BillNo { get; set; }

        public DateTime Date { get; set; }
        public int Amount { get; set; }
        public int Tax { get; set; }
        public int Dis { get; set; }
        public int Netamt { get; set; }
        public int TotAmt { get; set; }
        public int D { get; set; }
        public string Type { get; set; }
        public string Month { get; set; }
        public string Name { get; set; }
        public int ItemNo { get; set; }
        public int Rate { get; set; }
        public int Qty { get; set; }
    }
}
