﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public class ledgerTable
    {
        public int ledgerTableId { get; set; }
        [Key]
        public DateTime Date { get; set; }
        public string ExpName { get; set; }
        public string Des { get; set; }
        public int Eno { get; set; }
        public string Under { get; set; }
        public string Type { get; set; }
        public string Payment { get; set; }
        public string Type1 { get; set; }
        public int Debit { get; set; }
        public int Credit { get; set; }
        public string SalesMan { get; set; }
        public string CompanyName { get; set; }
    }
}
