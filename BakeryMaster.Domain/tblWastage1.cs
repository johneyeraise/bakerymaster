﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
   public class tblWastage1
    {
        public int wastageId { get; set; }
        [key]
        public int Vno { get; set; }
        public DateTime Invdate { get; set; }
        public string Labour { get; set; }
        public string ProdCode { get; set; }
        public string ItemName { get; set; }
        public string Category { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int Qty { get; set; }
        public string Reason { get; set; }
        public int Rate { get; set; }
    }

}
