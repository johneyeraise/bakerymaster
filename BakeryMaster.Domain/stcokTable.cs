﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BakeryMaster.Domain
{
    public class stcokTable
    {
        public int stcokTableID { get; set; }
        [Key]
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int Qty { get; set; }
        public string Type { get; set; }
        public int Rate { get; set; }
    }
}
